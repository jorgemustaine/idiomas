# English NoteBook

## Terminaciones verbales

### ING

Indica la acción de la cosa señalada por la palabra.


## Interesting links:

* [Comparative Adjetives **er** **est**](https://www.skypemeeasyenglish.com/post/2014/12/20/big-bigger-biggest-comparative-adjectives)
* [Rules and lesson about Suffixes er and est](https://eps.schoolspecialty.com/EPS/media/Site-Resources/downloads/samples/1018m/pp_b-lesson_57.pdf)
* [Phrasal verbs, go out, get out, get off, leave](https://madridingles.net/go-out-get-out-get-off-go-abroad-leave/)

In the most of the case we used: **er** termination for comparative adjetive and **est**
for superlative.

The suffix **er** is used to compare two things while the suffix **est** is used to
compare three or more things.

* [Vaughan channel in YouTube](https://www.youtube.com/channel/UCWZPQFtmoAo5AJSNImJnkrQ)

@jorgemustaine 2020
