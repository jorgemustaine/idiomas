# Describe a picture

I selected the next picture:  [HERE](https://pixabay.com/illustrations/gondolier-boat-moon-water-night-2018052/)

The pic is really pretty and it display a man on a boat who's rowing towards the
horizon. In the far we can see the ful moon. The boat, the moon and the sailer are
in the first plane.

The colors in the pic are cold predominantly blue and gray that's because the pic
was took in the night.

I like a lot the texture in the photo, It's like i can touch the water or soak
in it. The details in the clothes of the man that too realistic. The moon is another
element that make me feel some mystic in the scene.

It's a great picture, for me it's a poem without words, a graphic poem.

jorgescalona 2020
