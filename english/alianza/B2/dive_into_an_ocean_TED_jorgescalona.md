# Unit 11 / Lesson E

**Choose a video and write a paragraph about it.**


I selected **Dive into an ocean photographer's world** video.

The video is of **Thomas Peschak** He's a Conservation Photographer.

He began talk about: How when he was a child took his first ocean pic. He Began
to diving when he was ten years old in a coral reef. He was awesome surrounded by
fish with all of colors of the rainbow.

He has forty years old and had the privilege of explore the oceans.

He say that can display great pics about the nature and the animals in their
environments and he believe that it's the real power behind of the conservation
pictures, because those pics can give us a new perspective of the unknown places
and their important.

He want create natural resilience through his pics and talks.
