# Describe your hometown

My natal **Barquisimeto** city is in Venezuela, it's the fourth city in importance
there, its location is strategically for be a food collection and distribution
center, there is a mayor market for the rest of the country. My hometown also
it's the musical city, there is a big music school and it's the cradel of great
musicians and the Youth Ochest.

There are a lot of commerce centers like malls and openair markets too, also a
few beautiful parks and sport centers, In the city we have two sport teams for
the professional leagues of soccer and baseball, their names are: "Guaros de Lara"
for the soccer team and "Cardenales de Lara" for the Baseball team.

The public transportation isn't realiable because the big project for that is not
ready yet (a trolleybus system), but almost all in the town have a car or another
vehicle like bicycle.

If would you like know more about you can visit the next link, or maybe ask me
anything:

https://en.wikipedia.org/wiki/Barquisimeto

I hope that you like my post, see you soon!
