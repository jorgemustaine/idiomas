**Montevideo Sunday September Twentieth

# Writing about an amazing place by jorgescalona

A great place to visit is in Venezuela, that site is the highest waterfall in the
world. Itś name is "Salto Ángel" but the original name is in Pemon language: 
"Karepakupai Meru", meaning "the fall from highest point", with a height of nine
hundred seventy nine (979) metres or three thousand two hundred twelve feets. In
"Canima" park, a UNESCO World Heritage site.

It's location is at north of amazon rainforest, in an state named "Bolivar", it's
an site really amazing, with a wide biome, and original people living there,
the ethnic group "Pemon".

I'm not have doubt about it's the most spectacular site in South America, It's
full of: rivers, mountains, jungle, animals. Their rivers are navigable. It's 
a magic place.

For more information about, i recommend to visit this site:
https://en.wikipedia.org/wiki/Angel_Falls

@jorgescalona 2020
